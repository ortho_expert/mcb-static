var isValidName = false
var isValidEmail = false
var isValidBody = false
var isSent = false
$(document).ready(function () {
  "use strict";
  var formInqury = $("#form-inqury");
  var formName   = $("#form-name");
  var formEmail  = $("#form-email");
  var formBody   = $("#form-body");
  var formSubmit = $("#form-submit");

  var formNameHelp  = $("#form-name-help");
  var formEmailHelp = $("#form-email-help");
  var formBodyHelp  = $("#form-body-help");

  emailjs.init("user_xoPvfVDlVGL8rz3JKdXts");

  formName.on("input", function() {
    if (isSent) {
      return
    }
    isValidName = 1 < formName.val().length;

    if(isValidName) {
      formNameHelp.css("display", "none");
    } else {
      formNameHelp.css("display", "block");
    }

    if (isValidName && isValidEmail && isValidBody) {
      formSubmit.prop("disabled", false);
      formSubmit.css("opacity", 1.0);
    } else {
      formSubmit.prop("disabled", true);
      formSubmit.css("opacity", 0.5);
    }
  })
  formEmail.on("input", function() {
    if (isSent) {
      return
    }
    var expression = /(?!.*\.{2})^([a-z\d!#$%&"*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&"*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i
    isValidEmail = expression.test(formEmail.val().toLowerCase())

    if(isValidEmail) {
      formEmailHelp.css("display", "none");
    } else {
      formEmailHelp.css("display", "block");
    }
    if (isValidName && isValidEmail && isValidBody) {
      formSubmit.prop("disabled", false);
      formSubmit.css("opacity", 1.0);
    } else {
      formSubmit.prop("disabled", true);
      formSubmit.css("opacity", 0.5);
    }
  })
  formBody.on("input", function() {
    if (isSent) {
      return
    }
    isValidBody = 1 < formBody.val().length;

    if(isValidBody) {
      formBodyHelp.css("display", "none");
    } else {
      formBodyHelp.css("display", "block");
    }
    if (isValidName && isValidEmail && isValidBody) {
      formSubmit.prop("disabled", false);
      formSubmit.css("opacity", 1.0);
    } else {
      formSubmit.prop("disabled", true);
      formSubmit.css("opacity", 0.5);
    }
  })
  formSubmit.on("click", function(evt) {
    evt.preventDefault();
    if (!isValidName) {
        return
    }
    if (!isValidEmail) {
        return
    }
    if (!isValidBody) {
        return
    }
    isSent = true
    formSubmit.prop("disabled", true);
    formSubmit.css("opacity", 0.5);
    formSubmit.html("送信中...");

    var params = {
      name: formName.val(),
      email: formEmail.val(),
      body: formBody.val()
    }
    emailjs.send("mailer-general", "inquiry-yakupro", params);
    setTimeout(function () {
      formSubmit.css("opacity", 1.0);
      formSubmit.html("送信されました");
    }, 3200);
  });
});